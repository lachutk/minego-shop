<?php
if(isset($_SESSION['online']))
{
 include "js.php";
  ?>
  <div id="main_menu">
<nav class="navbar navbar-dark navbar-expand-md bg-default justify-content-center">
    <a href="index.php" class="tp navbar-brand d-flex mr-auto ml-3"><i class="fab fa-hire-a-helper mr-1"></i>Minego</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
        <ul class="nav navbar-nav ml-auto justify-content-end">
            <li class="nav-item ml-2">
                <ul class="navbar-nav mt-2 mt-md-0">
                  <li class="nav-item">
                <a class="nav-link ml-2" href="produkty.php">Produkty</a>
                  </li>
                  <li class="nav-item">
                <a class="nav-link ml-2" href="wystaw.php">Wystaw Produkt</a>
                  </li>
                  <li class="nav-item">
                <a class="nav-link ml-2" href="kupione.php">Kupione</a>
                  </li>
                  <li class="nav-item">
                <a class="nav-link ml-2" href="reklamacje.php">Reklamacje</a>
                  </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="userMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-user mr-1"></i>login/email
                  </a>
                  <div class="dropdown-menu" aria-labelledby="userMenu">
                    <a class="dropdown-item" href="registration/logout.php">
                      <i class="fas fa-sign-out-alt mr-1"></i>Wyloguj!</a>
                  </div>
                </li>
              </ul>
            
    </div>
</nav>
</div>
<?php } else {include "l_menu.php";} ?>