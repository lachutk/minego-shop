<?php
session_start();
$login_ip = $_SERVER['REMOTE_ADDR'];
if((!isset($_POST['login'])) || (!isset($_POST['password'])))
{
	header('Location: ../ind.php');
	exit();
}
require_once "../connect.php";

try 
{
      $connect = new mysqli($host, $db_user, $db_password, $db_name);
      if($connect->connect_errno!=0)
      {
        throw new Exception(mysqli_connect_errno()); //rzuć nowym wyjątkiem
      }
	else
	{
	$login = $_POST['login'];
	$password = $_POST['password'];


	$login = htmlentities($login, ENT_QUOTES, "UTF-8");

	if($result = @$connect->query(
		sprintf("SELECT * FROM users WHERE user='%s'",
	mysqli_real_escape_string($connect,$login))))
	{
		$how_users=$result->num_rows;
		if($how_users>0)
		{

			$row = $result->fetch_assoc();
			$hashp=sha1($password);

			if($hashp==$row['password'])
			{
				$_SESSION['online'] = true;

				
				$_SESSION['id'] = $row['id_user'];
				$_SESSION['user'] = $row['user'];
				$_SESSION['password'] = $row['password'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['date_of_registration'] = $row['date_of_registration'];
				$_SESSION['ip'] = $row['ip'];
				unset($_SESSION['erron']);
				$result->free_result();
				header('Location: ../index.php');
		}
		else
		{
				$_SESSION['erron']='Nieprawidłowy loign lub hasło!';
				header('Location: ../index.php');
		}
		}else
		{
			$_SESSION['erron']='Nieprawidłowy loign lub hasło!';
			header('Location: ../index.php');
		}

	}

	$connect->close();
}
}
catch(Exception $e) //złap wyjątki, jeśli jakieś zostały rzucone
  {
    echo '<center><br /><br /><br /><div class="error">Błąd serwera! Przepraszamy za niedogodności i prosimy o próbę logowania w innym terminie!</div>';
    echo '<br />Informacja developerska: '.$e.'</center>';

  }

?>